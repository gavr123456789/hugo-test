---
title: Оптимизация путем векторизации
subtitle: Итак, нам нужно инкрементировать массив элементов
date: 2019-07-07
tags: ["guide","example"]
---
![](http://funday.com.ua/uploads/posts/2009-06/1245747748_many_11.jpg)  

<!--more-->  

Итак, нам нужно инкрементировать массив элементов(увеличить каждое его значение на n)

Обычно это выглядит так

![](https://pp.userapi.com/c844720/v844720000/16a3b1/8iJ415srLF4.jpg)

Ннно, в современных процессорах есть векторные юниты… вообщем проще вики копирнуть

**SIMD**  ([англ.](https://vk.com/away.php?to=https%3A%2F%2Fru.wikipedia.org%2Fwiki%2F%C0%ED%E3%EB%E8%E9%F1%EA%E8%E9_%FF%E7%FB%EA&cc_key=) single instruction, multiple data —  **одиночный поток команд, множественный поток данных** ,  **ОКМД** ) — принцип компьютерных вычислений, позволяющий обеспечить [параллелизм](https://vk.com/away.php?to=https%3A%2F%2Fru.wikipedia.org%2Fwiki%2F%CF%E0%F0%E0%EB%EB%E5%EB%E8%E7%EC_%28%E8%ED%F4%EE%F0%EC%E0%F2%E8%EA%E0%29&cc_key=) на уровне данных. Один из [классов вычислительных систем](https://vk.com/away.php?to=https%3A%2F%2Fru.wikipedia.org%2Fwiki%2F%CA%EB%E0%F1%F1%E8%F4%E8%EA%E0%F6%E8%FF_%EF%E0%F0%E0%EB%EB%E5%EB%FC%ED%FB%F5_%E2%FB%F7%E8%F1%EB%E8%F2%E5%EB%FC%ED%FB%F5_%F1%E8%F1%F2%E5%EC&cc_key=) в [классификации Флинна](https://vk.com/away.php?to=https%3A%2F%2Fru.wikipedia.org%2Fwiki%2F%D2%E0%EA%F1%EE%ED%EE%EC%E8%FF_%D4%EB%E8%ED%ED%E0&cc_key=).

SIMD-компьютеры состоят из одного командного процессора (управляющего модуля), называемого контроллером, и нескольких модулей обработки данных, называемых процессорными элементами. Управляющий модуль принимает, анализирует и выполняет команды. Если в команде встречаются данные, контроллер рассылает на все процессорные элементы команду, и эта команда выполняется на нескольких или на всех процессорных элементах. Каждый процессорный элемент имеет свою собственную память для хранения данных. Одним из преимуществ данной архитектуры считается то, что в этом случае более эффективно реализована логика вычислений. До половины логических инструкций обычного процессора связано с управлением выполнением машинных команд, а остальная их часть относится к работе с внутренней памятью процессора и выполнению арифметических операций. В SIMD-компьютере управление выполняется контроллером, а «арифметика» отдана процессорным элементам.

[Векторные процессоры](https://vk.com/away.php?to=https%3A%2F%2Fru.wikipedia.org%2Fwiki%2F%C2%E5%EA%F2%EE%F0%ED%FB%E9_%EF%F0%EE%F6%E5%F1%F1%EE%F0&cc_key=) также использовали принцип SIMD, одной командой могли обрабатываться векторы размером до нескольких тысяч элементов.

Тк воооот, зная это, можно выполнить оптимизацию путем векторизации, тоесть загрузить эти самые регистры роботой:  
Было:

![](https://pp.userapi.com/c844720/v844720000/16a3b8/oPAlORPghWM.jpg)


Стало:
![](https://pp.userapi.com/c844720/v844720000/16a3c0/ILqmYrmxhOQ.jpg)


То есть теперь мы итерируем не по одному эл массива, а сразу по 4  
(`i+=4` вместо `i++`)  
и чтобы не прибавить а к несуществующим элементам `i<arr.length-4`

(тоесть вот нам пришел массив из 10, произойдет 3 итерации 0,4,8 прибавляем числа мы к след 4 зачением, тоесть 0,1,2,3; 4,5,6,7; 8,9,10,11 это будет выглядеть так

![](https://pp.userapi.com/c844720/v844720000/16a3e3/AGytcO255vs.jpg)

Произошло прибавление а к несуществующим элементам, то есть к рандомным участкам памяти, что не есть хорошо, но программа не вылетает потому что gcc немного умный

![](https://pp.userapi.com/c844720/v844720000/16a3eb/LU83bX49w78.jpg)

Это было объяснение почему не вылетает с `i<arr.length` без `-4;`)

Нам надо доитерировать остаток массива от деления на 4, тоесть из 10 мы проитерировали первые 8, осталось 9 10

![](https://pp.userapi.com/c844720/v844720000/16a3f9/EhB09TDm9Lg.jpg)

Ну а теперь только замерить время обычного способа и с оптимизацией.

Итак массив из 999999 заполняем рандомом, засекаем время, инкрементируем, смотрим скоко времени прошло:

![](https://pp.userapi.com/c844720/v844720000/16a401/NhtJ0_PXBGM.jpg)

![](https://pp.userapi.com/c844720/v844720000/16a40a/pduLNSKhweI.jpg)

значит в среднем чето около 7к микросекунд

Теперь закоментим обычный способ и раскоментим тот который складывает по 4 за раз, а потом остаток:

![](https://pp.userapi.com/c844720/v844720000/16a413/dnXGxqOpmXY.jpg)

Разница очевидна:

![](https://pp.userapi.com/c844720/v844720000/16a425/LBeSP-Waqd4.jpg)

теперь среднее на глаз 3,5 что в два раза быстрее.

Попробуем прибавлять не по 4 за раз а по 8, вдруг этих регистров там многа)

![](https://pp.userapi.com/c844720/v844720000/16a42e/lBaByEIbxTM.jpg)

Прирост поменьше, но он есть

![](https://pp.userapi.com/c844720/v844720000/16a43b/fJwn8pY3wo8.jpg)

Думаю продолжить эксперементы сможете сами, «а на сегодня все, до новых встреч» 