---
title: Возможности GLib
subtitle: GLib - Vala Core! Dir & Timer
date: 2019-05-21
tags: ["vala","guide","GLib","example"]
---

![](https://www.gtk.org/images/architecture.png)  

Такс, я потихоньку буду разбирать классы GLib, потому что GLib - базовая библиотека Vala, и все что умеет она умеет и Vala, тк сказать из коробки(без подключения сторонних библиотек).  
Я попытаюсь быть максимально краток и просто разбирать базовые примеры использования, а для тех кто хочет разобраться более детально буду оставлять ссылку на ValaDoc

<!--more-->

Компиляция любого кода из примеров всегда будет выглядеть одинакого `vala filename`

# GLib.Timer

И первым будет класс таймер, им можно легко засекать время. Отлично подходит для бэнчмарков.

Таймер автоматически начинает отсчет при создании объекта.

### Методы
1) `elapsed` - возвращает кол во времени в секундах double, а так же может принять ulong и заполнить его микросекундами, но это не обязательно (строки 10, 17, 26)  
2. `stop`   
3. `@continue` собака потому что это ключевое слово  
4. `start`  
5. `reset` - бессмысленная функция, ведь можно вызывать start на уже запущенный таймер, это даст тот же эффект.   

{{< highlight vala "linenos=table" >}}
public static int main (string[] args) {
	ulong microseconds;
	double seconds;

	Timer timer = new Timer ();

	for (int i = 0; i < 10000; i++);
	timer.stop ();

 	seconds = timer.elapsed (out microseconds);
	print(@"sec = $(seconds.to_string ()) msec = $microseconds\n");

	timer.@continue ();
	for (int i = 0; i < 100000; i++);
	timer.stop ();

 	seconds = timer.elapsed ();
	print(@"sec = $(seconds.to_string ())\n");

	timer.reset ();
	for (int i = 0; i < 10000; i++);
	for (int i = 0; i < 100000; i++);
	timer.stop ();

 	seconds = timer.elapsed ();
	print(@"sec = $(seconds.to_string ())\n");
	return 0;
}
{{< / highlight >}}

# Dir

{{< highlight go "linenos=table,hl_lines=3-4 8 11 14 17 20 " >}}
void main (string[] args) {
	try {
		string directory = "./";
		Dir dir = Dir.open (directory, 0);
		string? name = null;

		while ((name = dir.read_name ()) != null) {
			string path = Path.build_filename (directory, name);
			string type = "";

			if (FileUtils.test (path, FileTest.IS_REGULAR)) 
				type += "| REGULAR ";
			
			if (FileUtils.test (path, FileTest.IS_SYMLINK)) 
				type += "| SYMLINK ";
			
			if (FileUtils.test (path, FileTest.IS_DIR)) 
				type += "| DIR ";
			
			if (FileUtils.test (path, FileTest.IS_EXECUTABLE)) 
				type += "| EXECUTABLE ";
			
			stdout.printf(@"$name\t$type\n");
		}
	} catch (FileError err) {
		stderr.printf (err.message);
	}
}
{{< / highlight >}}

 - Строки **3-4** открытие текущей директории, второй параметр флаги(0 = без флагов)

 - **7-24** цикл который будет крутится пока в текущем каталоге не закончатся файлы, тк кк `read_name` возвращает следующий файл из текущего каталога, что-то вроде итератора, но для папок.  

 - **8** `Path.build_filename` строит путь из переданных ему аргументов, с учетом особенностей ОС (например на Windows в пути могут использоваться обратные слеши \ а не обычные)  
 - *Пример* `Path.build_filename ("my", "full", "path/to.txt");` даст `my/full/path/to.txt` на UNIX подобных ОС.  

 - **if'ы** - [FileUtils.test](https://valadoc.org/glib-2.0/GLib.FileUtils.test.html) возвращает true в случае если тесты указанные в его аргументах прошли для path. В Windows нет символических ссылок, поэтому тестирование `IS_SYMLINK` всегда будет возвращать false 
В случае если тест прошел к type добавляется собственно то, на что был тест. 

## Вывод 

  папка DIR           | EXECUTABLE | . | .
  --------------------|------------|----|----
  исполнительный файл | REGULAR    | EXECUTABLE| -
  timer.vala          | REGULAR    | - | -
  path_build_name.vala| REGULAR    | - | -
  Ссылка на исполнительный файл| REGULAR    | SYMLINK | EXECUTABLE
  path_build_name.vala| REGULAR    | - | - 
  

[Dir на ValaDoc](https://valadoc.org/glib-2.0/GLib.Dir.html)