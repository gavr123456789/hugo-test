---
title: Все полезные ссылки
subtitle: в большом количестве
date: 2019-04-20
tags: ["vala","info"]
---
<!--more-->
## Основные
1. [О Vala часть 1](https://docs.google.com/document/d/1y3e0EMzs7VhX7qsURSgJZlS_oyLzxVT31CcBBhfLN5M/edit?usp=sharing), [Лекция 1 О Vala](https://vk.com/@valalanguage-lekciya-1-o-vala) 
1. [Лекция 2 GTK](https://docs.google.com/document/d/1YQAgTinODtrINGQWnsHW-Zq820ImCR6MTWLrlRDGD24/edit?usp=sharing) пишем [Hello World на GTK](http://vk.com/@valalanguage-gtk-0), [Пример сигналов и слотов](https://vk.com/@valalanguage-sd)
1. [Типы данных](http://vk.com/@valalanguage-lekciya-3-tipy-dannyh-v-osnovnom-stroki), [Ещё типы данных](http://vk.com/@valalanguage-uchebnik-vala-osnovy1)
1. 5 [Виджеты](http://vk.com/@valalanguage-lekciya-56-vidzhety-tablica) 
6. [Дизайнер интерфейса/Runtime/Design Time](http://vk.com/@valalanguage-lekciya-6-runtimedesign-time)
1. [Коллекции Gee](http://vk.com/@valalanguage-lekciya-8-gee-biblioteka-kollekcii)
1. [Async, Thread](https://vk.com/@valalanguage-lekciya-7-asincsync-main-loop-thread)
1. [GIO много часть 1](http://vk.com/@valalanguage-biblioteka-vvoda-vyvoda-gio), [GIO Примеры кода часть 2](http://vk.com/@valalanguage-biblioteka-vvoda-vyvoda-gio-chast-2)
1. [Сети](https://vk.com/@valalanguage-lekciya-10-seti) 
1. [Мультимедиа](http://vk.com/@valalanguage-lekciya-11-multimedia), [Время и дата примеры кода](http://vk.com/@valalanguage-vremya-i-data)
1. [Реляционные базы данных](http://vk.com/@valalanguage-vala-sql-i-relyacionnye-bazy-dannyh)
1. [Lua и скриптовые динамические языки](http://vk.com/@valalanguage-lekciya-13-lua)
1. [Unit тестирование](http://vk.com/@valalanguage-vala-sql-i-relyacionnye-bazy-dannyh), [Контрактное программирование, логирование, обработка ошибок](http://vk.com/@valalanguage-vala-logirovanie-kontraknoe-programmirovanie-obrabotka-oshi)
1. [ООП](https://wiki.gnome.org/Projects/Vala/Tutorial/ru#Object_Oriented_Programming)(Переведено 50%), [ Get/Set Аксессоры](http://vk.com/@valalanguage-vala-logirovanie-kontraknoe-programmirovanie-obrabotka-oshi)
1. [JSON, XML, GSettings](https://vk.com/@valalanguage-jsonxml)

## Практические задания gif  
[TextView RLE](https://yapx.ru/v/DfL93)

## Полезные ссылки  
* [Самый основной учебник](https://wiki.gnome.org/Projects/Vala/Tutorial/ru)
* [Доки по всему иногда с примерами кода](https://valadoc.org/index.htm)
* [Базовые примеры матемаки, Thread, запись чтение в файл, ввод/вывод, класс](https://wiki.gnome.org/Projects/Vala/BasicSample/ru)
* [Установка на все ОС](https://wiki.gnome.org/Projects/Vala/ValaPlatforms)
* [Поддерживаемые IDE](https://wiki.gnome.org/Projects/Vala/Tools)
* [Примеры использования всех библиотек](https://wiki.gnome.org/Projects/Vala/Documentation)
* [Туториал для тех кто уже знает Java](https://wiki.gnome.org/Projects/Vala/Tutorial/ru)
* [Туториал для тех кто уже знает С#](https://wiki.gnome.org/Projects/Vala/ValaForCSharpProgrammers)
* [Плей-лист уроков по Vala + GTK](https://www.youtube.com/watch?v=7z0NVCrJr6A&list=PLriKzYyLb28mn2lS3c5yqMHgLREi7kR9-)
* [Примеры использования всех основных виджетов](https://developer.gnome.org/gnome-devel-demos/stable/beginner.vala.html.en), [Еще примеры виджетов](https://github.com/gerito1/vala-gtk-examples)
* [Вызов Vala кода из других ЯП](https://github.com/antono/vala-object)
* [Creating a Shared Library in Vala](https://wiki.gnome.org/Projects/Vala/SharedLibSample)
* [Все примеры кода с valaDoc отдельно](https://github.com/Valadoc/valadoc-org/tree/master/examples)
* [Почему Vala не ЯП в обычном понимании(пока что англ)](https://blogs.gnome.org/despinosa/2017/02/14/vala-is-not-a-programming-language/)
* [Чем круты интерфейсы в Vala](https://blogs.gnome.org/jnelson/2011/11/01/a-few-of-my-favorite-vala-things-interface/)


## Самые основные библиотеки  
* [GLib](https://valadoc.org/glib-2.0/index.htm) - Базовая библиотека Vala([wiki GLib](https://ru.wikipedia.org/wiki/GLib))
* [Gee](https://valadoc.org/gee-0.8/index.htm) - Библиотека коллекций
* [Gio](https://valadoc.org/gio-2.0/index.htm) - Огромная базовая библиотека ввода/вывода (там же поддержка асинхронности, там же работа с сокетами) [Описание на русском](https://ru.bmstu.wiki/GIO_(Gnome_Input/Output))
* [GTK](https://valadoc.org/gtk+-3.0/index.htm) - Кроссплатформенный GUI фреймворк
* [GSL](https://valadoc.org/gsl/index.htm) - GNU Scientific Library - невероятное количество математики [доки](https://www.gnu.org/software/gsl/)
* [Ambition](http://www.ambitionframework.org/wiki/etc/faq) - Веб-фреймворк по модели [MVC](https://ru.wikipedia.org/wiki/Model-View-Controller). [Пример](http://www.ambitionframework.org/wiki/tutorial/blog) создания простого блога 
* [Valum](https://valum-framework.readthedocs.io/en/stable/getting-started/) - еще один веб-фреймворк
* [Коллекция VAPI](https://github.com/nemequ/vala-girs)  

## Интересные проекты  
* [Круглый виджет проегресса](https://github.com/phastmike/vala-circular-progress-bar)
* [Нечто суъезное, надо разбираться](https://github.com/roojs/roobuilder) - походу это web-Vala IDE
* [Паттерны разработки for Humans](https://github.com/phastmike/vala-design-patterns-for-humans)
* [Биндинги к CEF(chromium)](https://github.com/tiliado/valacef)
* [Виджет таблички, судя по всему](https://github.com/Df458/dflib)
* [Набор мелких приложений написанных на Vala, отлично подойдет для обучения](https://github.com/simargl/vala-apps)
* [Виджеты графиков](https://github.com/dcharles525/Caroline)
