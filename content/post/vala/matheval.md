---
title: Выполняем математику из строки
subtitle: Lib Math EVAL!
date: 2019-05-07
tags: ["vala","example","math"]
---

![](https://www.yu.edu/sites/default/files/math-515606506.jpg)  

В вашем приложении вам понадобилось выполнять математику из строки, тогда библиотека matheval это то что вам нужно!
<!--more-->

# Выполняем математику из строки или калькулятор в 30 строк кода

1) Установить libmatheval. [Valadoc](https://vk.com/away.php?to=https%3A%2F%2Fvaladoc.org%2Flibmatheval%2Fmatheval.Evaluator.html&cc_key=) с описанием функций

![](https://pp.userapi.com/c851336/v851336668/100a3c/VX38MEt4YcY.jpg)

2) Создайте файл [libmatheval.vapi](https://vk.com/away.php?to=https%3A%2F%2Fgithub.com%2Fnemequ%2Fvala-extra-vapis%2Fblob%2Fmaster%2Flibmatheval.vapi&cc_key=)

3) Скопируйте его в /usr/share/vala/vapi (здесь хранятся все vapi(то что подключается когда делаешь using * ))

Создам тестовую программу

![](https://pp.userapi.com/c851336/v851336259/101869/lBXip1nEq3M.jpg)

Думаю проще уже некуда, проверим длинную арифметику.

![](https://pp.userapi.com/c851336/v851336259/101871/kzk1JDL_wnA.jpg)

![](https://pp.userapi.com/c851336/v851336259/101892/JHv3HNU7gow.jpg)

А теперь простейший GUI калькулятор

![](https://pp.userapi.com/c851336/v851336259/1018b1/NJn1KAQcLMA.jpg)

![](https://pp.userapi.com/c851336/v851336259/1018a2/aqs3CrdeCFU.jpg)

![](https://pp.userapi.com/c851336/v851336259/1018a9/Qg9sM0TMjeA.jpg)

## Возможности

что умеет в формате: название (@название которое нужно использовать для вызова в коде)
```vala
Supported elementary functions are (names that should be used are given
in parenthesis): exponential (@code{exp}), logarithmic (@code{log}),
square root (@code{sqrt}), sine (@code{sin}), cosine (@code{cos}),
tangent (@code{tan}), cotangent (@code{cot}), secant (@code{sec}),
cosecant (@code{csc}), inverse sine (@code{asin}), inverse cosine
(@code{acos}), inverse tangent (@code{atan}), inverse cotangent
(@code{acot}), inverse secant (@code{asec}), inverse cosecant
(@code{acsc}), hyperbolic sine (@code{sinh}), cosine (@code{cosh}),
hyperbolic tangent (@code{tanh}), hyperbolic cotangent (@code{coth}),
hyperbolic secant (@code{sech}), hyperbolic cosecant (@code{csch}),
hyperbolic inverse sine (@code{asinh}), hyperbolic inverse cosine
(@code{acosh}), hyperbolic inverse tangent (@code{atanh}), hyperbolic
inverse cotangent (@code{acoth}), hyperbolic inverse secant
(@code{asech}), hyperbolic inverse cosecant (@code{acsch}), absolute
value (@code{abs}), Heaviside step function (@code{step}) with value 1
defined for x = 0, Dirac delta function with infinity (@code{delta}) and
not-a-number (@code{nandelta}) values defined for x = 0, and error
function (@code{erf}).

Supported constants are (names that should be used are given in
parenthesis): e (@code{e}), log2(e) (@code{log2e}), log10(e)
(@code{log10e}), ln(2) (@code{ln2}), ln(10) (@code{ln10}), pi
(@code{pi}), pi / 2 (@code{pi_2}), pi / 4 (@code{pi_4}), 1 / pi
(@code{1_pi}), 2 / pi (@code{2_pi}), 2 / sqrt(pi) (@code{2_sqrtpi}),
sqrt(2) (@code{sqrt}) and sqrt(1 / 2) (@code{sqrt1_2})
```

Также существует возможность использовать переменные, вот пример:

![](https://pp.userapi.com/c851336/v851336491/1052b4/QbvqcRMh5Z8.jpg)

Для задания собственных имен нужно передать (массив имен, массив переменных)

PS заметил весьма интересные вещи, Vala автоматически приводит к значению в которое поместится переменная:

![](https://pp.userapi.com/c851336/v851336259/101879/9mSy0sITXK0.jpg)

Вот это не работает

![](https://pp.userapi.com/c851336/v851336259/101881/e1vV81-XRpg.jpg)

а вот тут запросто


[Vala language](https://vk.com/valalanguage)