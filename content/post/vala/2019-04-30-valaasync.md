---
title: Немного асинхронщины
subtitle: Пять строк, пять! Ну возьми две! Ну выпей, выпей на работе, что тебе стоит блядь. Пять строк!
date: 2019-05-06
tags: ["vala","example"]
---

![](https://pp.userapi.com/c853420/v853420983/38275/clQrTYDcwZI.jpg)

 Когда у нас возникает потребность асинхронно запустить какую то прогу из нашей, то в 90% случаев нам нужно узнать что она выведет, или по крайней мере когда завершится.  

<!--more-->



## Обычная асинхронность

Просто асинхронно запустить что-то в Vala очень просто:  
![](https://pp.userapi.com/c849220/v849220223/184bf4/vPcBiKk4eGU.jpg)

Первый аргумент это путь в каком каталоге запустить команду, второй это собственно сама команда и ее флаги, третий - получение $PATH чтобы не нужно было указывать путь до bin в первом аргументе, далее флаги, например есть флаг который позволяет чтение или запись в поток([все виды флагов](https://vk.com/away.php?to=https%3A%2F%2Fvaladoc.org%2Fglib-2.0%2FGLib.SpawnFlags.html&cc_key=)), предпоследний аргумент (который  `null` ) это user_data которую вы бы хотели принять на той стороне, и последний аргумент это пид то есть уникальный индификатор, который ОС дает каждому процессу(можно сказать именно этим потоки отличаются от процессов, но нет, у процесса могут быть потоки, а у потока не может быть процессов)

Далее тут через лямбюду добавляется ChildWatch.add -- реакция на событие завершения запущенного процесса.

Подытожу с помощью этого всего мы можем среагировать на завершение запущенной асинхронной проги, но допустим если это WGET который скачивает файл, без технологии пайпов мы никак не сможем получить вывод. Например процентов закачки.

UPD: И только сегодня я заметил что все это было про команду spawn_async, то есть универсальный спавн чего то асинхронного, существует также [spawn_comand_async](https://valadoc.org/glib-2.0/GLib.Process.spawn_command_line_async.html) пример использования которой выглядит гораздо короче, но вообще не дает нам никаких возможностей.

```vala
public static int main (string[] args) {
	try {
		Process.spawn_command_line_async ("mkdir MY-NEW-DIR");
	} catch (SpawnError e) {
		print ("Error: %s\n", e.message);
	}
	return 0;
}
//valac --pkg glib-2.0 GLib.Process.spawn_command_line_async.vala
```

## Pipes

wiki

В [программировании](https://vk.com/away.php?to=https%3A%2F%2Fru.wikipedia.org%2Fwiki%2F%CF%F0%EE%E3%F0%E0%EC%EC%E8%F0%EE%E2%E0%ED%E8%E5&cc_key=)  **именованный канал**  или  **именованный конвейер** ([англ.](https://vk.com/away.php?to=https%3A%2F%2Fru.wikipedia.org%2Fwiki%2F%C0%ED%E3%EB%E8%E9%F1%EA%E8%E9_%FF%E7%FB%EA&cc_key=) named pipe) — один из методов [межпроцессного взаимодействия](https://vk.com/away.php?to=https%3A%2F%2Fru.wikipedia.org%2Fwiki%2F%CC%E5%E6%EF%F0%EE%F6%E5%F1%F1%ED%EE%E5_%E2%E7%E0%E8%EC%EE%E4%E5%E9%F1%F2%E2%E8%E5&cc_key=), расширение понятия [конвейера](https://vk.com/away.php?to=https%3A%2F%2Fru.wikipedia.org%2Fwiki%2F%CA%EE%ED%E2%E5%E9%E5%F0_%28UNIX%29&cc_key=) в [Unix](https://vk.com/away.php?to=https%3A%2F%2Fru.wikipedia.org%2Fwiki%2FUnix&cc_key=) и подобных [ОС](https://vk.com/away.php?to=https%3A%2F%2Fru.wikipedia.org%2Fwiki%2F%CE%EF%E5%F0%E0%F6%E8%EE%ED%ED%E0%FF_%F1%E8%F1%F2%E5%EC%E0&cc_key=). Именованный канал позволяет различным процессам обмениваться данными, даже если программы, выполняющиеся в этих процессах, изначально не были написаны для взаимодействия с другими программами. Это понятие также существует и в [Microsoft Windows](https://vk.com/away.php?to=https%3A%2F%2Fru.wikipedia.org%2Fwiki%2FMicrosoft_Windows&cc_key=), хотя там его [семантика](https://vk.com/away.php?to=https%3A%2F%2Fru.wikipedia.org%2Fwiki%2F%D1%E5%EC%E0%ED%F2%E8%EA%E0&cc_key=) существенно отличается. Традиционный канал — «безымянен», потому что существует анонимно и только во время выполнения процесса. Именованный канал — существует в [системе](https://vk.com/away.php?to=https%3A%2F%2Fru.wikipedia.org%2Fwiki%2F%CE%EF%E5%F0%E0%F6%E8%EE%ED%ED%E0%FF_%F1%E8%F1%F2%E5%EC%E0&cc_key=) и после завершения процесса. Он должен быть «отсоединён» или удалён, когда уже не используется. Процессы обычно подсоединяются к каналу для осуществления взаимодействия между ними.

я

Короче это способ присосаца к запущенной проге и читать её выхлоп, даже если в ней не предусмотрена эта возможность(см [сокеты](https://ru.wikipedia.org/wiki/%D0%A1%D0%BE%D0%BA%D0%B5%D1%82_(%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%BD%D1%8B%D0%B9_%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D1%84%D0%B5%D0%B9%D1%81))

## Гранит и асинхронность с пайпами.

В Vala также существует spawn_async_with_pipes

Я не буду объяснять пример её использования, он в любом случае не поместится на скрин. Если интересно вот [тут](https://vk.com/away.php?to=https%3A%2F%2Fvaladoc.org%2Fglib-2.0%2FGLib.Process.spawn_async_with_pipes.html&cc_key=) есть просто тонны текста и сам пример.  
Основной вывод — это неудобна, хачу короче.

Иии библиотека гранит позволяет организовать это в 5 строк кода.

На самом деле гранит это фреймворк над GTK, а SimpleComand просто является одной из ее фич.

Значт так, первый аргумент - путь, второй команда. Всё, осталось только сделать  `run();`

### Содержание класса:

![](https://pp.userapi.com/c855724/v855724377/38a22/9J-o0KbsKUo.jpg)

А всё самое необходимое. Сигнал done о том что процесс завершился, отлов ерроров и оутпут, причем не только в качестве полей, но и самое главное сигналов, значит можно будет читать все в реальном времени, а не после завершения.

## Пример использования

Попробуем для начала просто ls. Конектим сигнал output_changed с нашей функцией, которая просто будет выводить то что получила.

Итак вот простейшая программа с гуи которая делает ls в корне и выводит в виджет с текстом

![](https://pp.userapi.com/c855724/v855724863/38092/X67Cm7QyjLA.jpg)

Эти 5 строк целиком отвечают за асинхронный запуск программы ls, отлов каждого изменения в ее выхлопе, и добавление этого в конец буфера виджета текста.

Думаю коментарии излишни, это невероятно просто по сравнению с тем что предлагает spawn_async_with_pipes из GLib, да и с другими языками программирования. 
Собственно [SimpleComand](https://vk.com/away.php?to=https%3A%2F%2Fvaladoc.org%2Fgranite%2FGranite.Services.SimpleCommand.html&cc_key=) это wrapper(обертка), и в сурсах библиотеки как раз видно что там используется spawn_async_with_pipes, просто все сделано за нас.

## Код для копирования

```vala
//компилировать так valac 56.vala --pkg granite --pkg gtk+-3.0
using Gtk;
using Granite;
public class MyApplication : Gtk.Application {
	protected override void activate () {
		new MyWindow (this).show_all ();
	}
}
class MyWindow: Gtk.ApplicationWindow {
	internal MyWindow (MyApplication app) {
		Object (application: app, title: "Async Example");
		this.set_default_size (600, 400);

		var buffer = new Gtk.TextBuffer (null);
		var textview = new Gtk.TextView.with_buffer (buffer); 
		textview.set_wrap_mode (Gtk.WrapMode.WORD);

		var scrolled_window = new Gtk.ScrolledWindow (null, null);
		scrolled_window.set_policy (Gtk.PolicyType.AUTOMATIC,
		                            Gtk.PolicyType.AUTOMATIC);
		scrolled_window.add (textview);
		scrolled_window.set_border_width (5);		

		this.add (scrolled_window);
		
		var proc = new Granite.Services.SimpleCommand("/","/bin/ls");
		proc.output_changed.connect((str) => {
			buffer.insert_at_cursor(str,str.length);
		});
		proc.run();
	}
}
public int main (string[] args) {
	return new MyApplication ().run (args);
}
```

### Чей то коментарий с конфы по Vala

Supplementary to the above, if you aren't sure where the binary resides you can initially search the `PATH`. To get this var you can use `GLib.Environ` object